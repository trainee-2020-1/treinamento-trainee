var caixa1 = document.querySelector("#one");
var caixa2 = document.querySelector("#two");
var caixa3 = document.querySelector("#descricao");

/*                          Primeira Caixa                        */
caixa1.addEventListener("mouseenter", function () {
  entrar(caixa1);
});
caixa1.addEventListener("mouseleave", function () {
  sair(caixa1);
});

/*                          Segunda Caixa                        */
caixa2.addEventListener("mouseenter", function () {
  entrar(caixa2);
});
caixa2.addEventListener("mouseleave", function () {
  sair(caixa2);
});

/*                          Terceira Caixa                        */
caixa3.addEventListener("mouseenter", function () {
  entrar(caixa3);
});
caixa3.addEventListener("mouseleave", function () {
  sair(caixa3);
});

function entrar(e) {
  e.style.background = " rgb(164, 193, 255)";
}

function sair(s) {
  s.style.background = "#DFE6E9";
}


