Bem vindos ao repositório do projeto trainee
============================================
#### Aqui vai ser onde vocês vão utilizar para criar seus treinamento do trainee

# Como utilizar o git

### Passo a passo

1. [Instalar GIT](https://www.hostinger.com.br/tutoriais/tutorial-do-git-basics-introducao/)
2. Criar uma pasta no local de preferencia, clicar com o botão direito e abrir o terminal do Git (Git Bash)
3. Copie e cole os seguintes comando no terminal: ```
git clone https://gitlab.com/trainee-2020-1/treinamento-trainee.git | git config --global user.email "<seu_email>" | git config --global user.name "<seu_nome_de_usuario>"```

# Estrutura de pastas

### As pastas do projeto vão ficar estruturadas da seguinte maneira

* Para cada pagina HTML devera ser criada uma pasta com o nome da página (e.g contato) e dentro dela deverá conter o arquivo da pagina com o nome **index.hml** 
* A página inicial do site deve ficar dentro da pasta raiz, com o nome *index.html*
* Os arquivos de estilo devem ficar dentro da pasta **css** e eles devem ter os mesmos nomes que as pastas das páginas
* Arquivos Javascript devem ficar dentro da pasta **scripts**

##### Mapa das pastas
```bash
.
├── css
│   ├── index.css
│   └── realizar-pedido.css
├── index.html
├── realizar-pedido
│   └── index.html
└── scripts
```

# FAQ

1. Como faço para mandar as alterações que fiz para o git?
   * git add .
   * git commit -m "mensagem descritiva do que tu alterou" (e.g tela de pedidos)
   * git push (se for a primeira vez que for mandar coisa pro git usar git push -u origin master)
2. Como atualizar as a pasta do meu computador com as mudanças que as outras pessoas fizeram?
   * git pull

# Links úteis 

* [Como usar git](https://rogerdudler.github.io/git-guide/index.pt_BR.html)
* [Introdução ao git](https://medium.com/taciossbr/uma-introducao-ao-git-o-que-e-e-como-usar-1dd721a1e6b0)
